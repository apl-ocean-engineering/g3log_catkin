// Copyright 2017-2022 University of Washington
// Author: Aaron Marburg <amarburg@uw.edu>

#pragma once

#include <ros/console.h>

#include <g3log/g3log.hpp>
#include <string>

struct ROSLogSink {
  // This sink is a thin wrapper around the ROS logging functionality.
  // This class doesn't do any filtering ... let ROS handle it.
  explicit ROSLogSink(const LEVELS threshold = INFO) { ; }

  ~ROSLogSink() { ; }

  void setThreshold(const LEVELS t) { ; }

  void ReceiveLogMessage(g3::LogMessageMover logEntry) {
    auto level = logEntry.get()._level;

    // Rather than using G3log's default string formatting (msg.toString()),
    // only include the subset of metadata that's not duplicated by roslog.
    // (We include original function and file in the message since the data
    // published in rosgraph_msgs/Log will point to this file.)
    auto msg = logEntry.get();
    std::string entry;
    entry.append("[" + msg.file() + "->" + msg.function() + ":" + msg.line() +
                 "]: " + msg.message());

    if (level == WARNING) {
      ROS_WARN_STREAM(entry);
    } else if (level == DEBUG) {
      ROS_DEBUG_STREAM(entry);
    } else {
      ROS_INFO_STREAM(entry);
    }
  }

 private:
};
